#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "CAF::core" for configuration ""
set_property(TARGET CAF::core APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(CAF::core PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libcaf_core.0.18.5.dylib"
  IMPORTED_SONAME_NOCONFIG "@rpath/libcaf_core.0.18.5.dylib"
  )

list(APPEND _IMPORT_CHECK_TARGETS CAF::core )
list(APPEND _IMPORT_CHECK_FILES_FOR_CAF::core "${_IMPORT_PREFIX}/lib/libcaf_core.0.18.5.dylib" )

# Import target "CAF::io" for configuration ""
set_property(TARGET CAF::io APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(CAF::io PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libcaf_io.0.18.5.dylib"
  IMPORTED_SONAME_NOCONFIG "@rpath/libcaf_io.0.18.5.dylib"
  )

list(APPEND _IMPORT_CHECK_TARGETS CAF::io )
list(APPEND _IMPORT_CHECK_FILES_FOR_CAF::io "${_IMPORT_PREFIX}/lib/libcaf_io.0.18.5.dylib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
